<!DOCTYPE html>
<html>
<head>
<script src="<?=base_url()?>assets/backend/js/tinymce/tinymce.min.js?apiKey=l4ul9agzrme64nxywmrz12610dnqsj8nkjch3nqoxm0hc1wl"></script>
<script>
    tinymce.init({
        selector: '#mytextarea',
        height: 500,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste imagetools wordcount"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ]
    });
</script>
</head>
<body>
  <h1>TinyMCE Quick Start Guide</h1>
  <form method="post">
    <textarea id="mytextarea">Hello, World!</textarea>
  </form>
</body>
</html>