<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('frontend/inc/v_header.php');
    $this->load->view('frontend/inc/v_menu.php');
?>

        <!--================Home Banner Area =================-->
        <section class="banner_area">
            <div class="banner_inner d-flex align-items-center">
				<div class="container">
					<div class="banner_content text-center">
						<h2><?=$event->title?></h2>
					</div>
				</div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->
        
        <!--================Event Details Area =================-->
		<section class="event_details_area p_120">
			<div class="container">
				<div class="event_d_inner">
					<img class="img-fluid" src="<?php echo base_url(); ?>assets/uploads/<?=$event->main_image?>" alt="">
					<div class="row event_text_inner">
						<div class="col-lg-4">
							<div class="left_text">
								<ul class="list">
									<li><i class="lnr lnr-calendar-full"></i><?=$event->date?></li>
									<li><i class="lnr lnr-apartment"></i><?=$event->people?></li>
								</ul>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="right_text">
								<h4><?=$event->title?></h4>
								<?=$event->information?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
        <!--================End Event Details Area =================-->
