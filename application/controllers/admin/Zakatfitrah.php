<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zakatfitrah extends AdminInterface {
	function __construct(){
		parent::__construct();		
		$this->load->model('backend/m_zakat_fitrah');
	}

	public function index(){
		$data['zakat'] = $this->m_zakat_fitrah->view_zakat_data()->result();
		$this->load->view('backend/v_zakat_fitrah_view', $data);
	}

	public function add(){
		$this->load->view('backend/v_zakat_fitrah_add');
	}

	function exec_add(){
		$date = $this->input->post('date-zakat');
		$name = $this->input->post('name-zakat');
		$jiwa = $this->input->post('number-jiwa');
		$category = $this->input->post('category-zakat');
		$total = $this->input->post('number-zakat');
		$info = $this->input->post('text-zakat');

		$data = array(
			'date' => $date,
			'name' => $name,
			'jiwa' => $jiwa,
			'category' => $category,
			'total' => $total,
			'info' => $info
		);
		$this->m_zakat_fitrah->insert_zakat_data($data);
		redirect('admin/zakatfitrah/');
	}

	function delete($id){
		$where = array('id_zakat_fitrah' => $id);
		$this->m_zakat_fitrah->delete_zakat_data($where);
		redirect('admin/zakatfitrah/');
	}
	
}