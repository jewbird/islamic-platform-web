<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends AdminInterface {
	public function index(){
		$this->load->model('backend/m_infaq');
		$this->load->model('backend/m_zakat_fitrah');
		$this->load->model('backend/m_zakat_mal');
		$this->load->model('backend/m_users');
		$this->load->model('backend/m_takmir');
		$this->load->model('backend/m_event');
		$this->load->model('backend/m_news');

		// Total Infaq
		$where = array('category' => 'Infaq Mingguan');
		$data['infaq_mingguan'] = $this->m_infaq->get_total_infaq($where)->row_array()['total'];
		$where = array('category' => "Infaq Jum'at");
		$data['infaq_jumat'] = $this->m_infaq->get_total_infaq($where)->row_array()['total'];

		// Total Zakat
		$data['zakat_fitrah'] = $this->m_zakat_fitrah->get_total_zakat($where)->row_array()['total'];
		$data['zakat_mal'] = $this->m_zakat_mal->get_total_zakat_mal($where)->row_array()['total'];

		// Total Jama'ah
		$data['jamaah'] = $this->m_users->view_users_data()->num_rows();

		// Total Takmir
		$data['takmir'] = $this->m_takmir->view_takmir_data()->num_rows();

		// Total Kegiatan
		$data['kegiatan'] = $this->m_event->view_event_data()->num_rows();

		// Total Berita
		$data['berita'] = $this->m_news->view_news_data()->num_rows();

		// Sambutan
		$data['sambutan'] = $this->m_users->get_mosque()->row_array()['greeting'];

		$this->load->view('backend/v_dashboard', $data);
	}
}