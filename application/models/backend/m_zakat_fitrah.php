<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_zakat_fitrah extends CI_Model{
    function view_zakat_data(){
        return $this->db->get('mjd_zakat_fitrah');
    }
    
    function insert_zakat_data($data){
        $this->db->insert('mjd_zakat_fitrah',$data);
    }
    
    function delete_zakat_data($where){
        $this->db->where($where);
        $this->db->delete('mjd_zakat_fitrah');
    }
    
    function details_zakat_data($where){
        return $this->db->get_where('mjd_zakat_fitrah',$where);
    }
    
    function get_total_zakat($where) {
        $this->db->select_sum('total');
        return $this->db->get('mjd_zakat_fitrah');
    }
}