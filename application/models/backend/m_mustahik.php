<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_mustahik extends CI_Model{
	function view_mustahik_data(){
		return $this->db->get('mjd_mustahik');
	}

	function insert_mustahik_data($data){
		$this->db->insert('mjd_mustahik',$data);
    }

	function delete_mustahik_data($where){
		$this->db->where($where);
		$this->db->delete('mjd_mustahik');
	}

	function details_mustahik_data($where){		
		return $this->db->get_where('mjd_mustahik',$where);
  }
}