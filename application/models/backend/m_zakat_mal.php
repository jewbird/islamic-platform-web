<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_zakat_mal extends CI_Model{
    function view_zakat_mal_data(){
        return $this->db->get('mjd_zakat_mal');
    }
    
    function insert_zakat_mal_data($data){
        $this->db->insert('mjd_zakat_mal',$data);
    }
    
    function delete_zakat_mal_data($where){
        $this->db->where($where);
        $this->db->delete('mjd_zakat_mal');
    }
    
    function details_zakat_mal_data($where){
        return $this->db->get_where('mjd_zakat_mal',$where);
    }
    
    function get_total_zakat_mal($where) {
        $this->db->select_sum('total');
        return $this->db->get('mjd_zakat_mal');
    }
}